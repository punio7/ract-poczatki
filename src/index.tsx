import React from 'react';
import ReactDOM from 'react-dom';

const App = () => {
    return <div>my app</div>;
};

ReactDOM.render(<App />, document.getElementById('root'));
